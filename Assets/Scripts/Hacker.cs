﻿using UnityEngine;

public class Hacker : MonoBehaviour {
    //Config parms
    string menuHint = "You may type menu at any time.";
    string[] level1Passwords = { "book", "fine", "shelf", "card", "cart" };
    string[] level2Passwords = { "detective", "cell", "booking", "bail", "arrest", "warrant" };
    string[] level3Passwords = { "constellation", "telescope", "aeronautics", "andromeda", "planets", "astronauts" };

    // Game state
    int level;
    string password;

    enum Screen
    {
        MainMenu, Password, Win
    };
    Screen currentScreen = Screen.MainMenu;
	// Use this for initialization
	void Start ()
    {
        ShowMainMenu();
    }

    private void ShowMainMenu()
    {
        currentScreen = Screen.MainMenu;
        Terminal.ClearScreen();
        Terminal.WriteLine("What would you like to hack into?\n");
        Terminal.WriteLine("Press 1 for the local library");
        Terminal.WriteLine("Press 2 for the police station");
        Terminal.WriteLine("Press 3 for NASA\n");
        Terminal.WriteLine("Enter your selection: ");
    }

    void OnUserInput(string input)
    {
        if (input == "menu")
        {
            ShowMainMenu();
        }
        else if (input == "quit" || input == "close" || input == "exit")
        {
            Application.Quit();
        }
        else if (currentScreen == Screen.MainMenu)
        {
            RunMainMenu(input);
        }
        else if (currentScreen == Screen.Password)
        {
            CheckPassword(input);
        }
    }

    private void CheckPassword(string input)
    {

        if (input == password)
        {
            DisplayWinScreen();
        }
        else
        {
            Terminal.WriteLine("Sorry, wrong password!");
        }

    }

    private void DisplayWinScreen()
    {
        currentScreen = Screen.Win;
        Terminal.ClearScreen();
        ShowLevelReward();
        Terminal.WriteLine(menuHint);
    }

    private void ShowLevelReward()
    {
        switch (level)
        {
            case 1:
                Terminal.WriteLine("Have a book...");
                Terminal.WriteLine(@"
    _______
   /      //
  /      // 
 /_____ //
(______(/
");
                break;
            case 2:
                Terminal.WriteLine("You got the key!");
                Terminal.WriteLine(@"
 __
/o \_______
\__/-=' = '
");
                break;
           case 3:
                Terminal.WriteLine("Blast off!");
                Terminal.WriteLine(@"
      |      
     /_\
    |   |
   ,|   |.
  / | | | \
  |-'-|-'.|
");
                break;
            default:
                Debug.Log("Not implemented");
                break;
        }
    }

    private void RunMainMenu(string input)
    {
        if (input == "1" || input == "2" || input == "3")
        {
            level = int.Parse(input);
            AskForPassword(level);
        }
        else
        {
            Terminal.WriteLine("Please choose a valid level");
            Terminal.WriteLine(menuHint);
        }
    }

    private void AskForPassword(int level)
    {
        currentScreen = Screen.Password;
        Terminal.ClearScreen();
        SetRandomPassword(level);
        Terminal.WriteLine("Enter your password, hint: " + password.Anagram());
        Terminal.WriteLine(menuHint);
    }

    private void SetRandomPassword(int level)
    {
        switch (level)
        {
            case 1:
                password = level1Passwords[Random.Range(0, level1Passwords.Length)];
                break;
            case 2:
                password = level2Passwords[Random.Range(0, level2Passwords.Length)];
                break;
            case 3:
                password = level3Passwords[Random.Range(0, level3Passwords.Length)];
                break;
            default:
                Debug.LogError("Invalid level number");
                break;
        }
    }
}
